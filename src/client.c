#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <time.h>

#define CONNECT_PORT 80
#define MAX_RESPONSE_SIZE (1024*1024*5)
#define MAX_REQUEST_SIZE (1024)
#define MAX_HOSTNAME_SIZE 1024
#define MAX_PATH_SIZE 1024
#define MAX_SIZE 128
#define MAX_URL 256

struct results
{
    int64_t ping;
    int64_t uptime;
    int64_t downtime;
    int reqSize;
    int repSize;
};

/*
 * ホスト名からIPアドレスを取得する
 * hostname：ホスト名
 * 戻り値：IPアドレスが格納されたバッファへのアドレス（失敗時はNULL）
 */
char *getIpAddress(char *hostname) {
    struct hostent *host;

    /* ホスト名からホスト情報を取得 */
    host = gethostbyname(hostname);
    if (host == NULL) {
        printf("gethostbyname error\n");
        return NULL;
    }

    /* IPv4以外はエラー */
    if (host->h_length != 4) {
        printf("Not ipv4\n");
        return NULL;
    }

    /* ホスト情報のアドレス群の１つ目をIPアドレスとする */
    return host->h_addr_list[0];
}

/*
 * URLからホスト名とパスを取得する
 * hostname：ホスト名を格納するバッファへのアドレス
 * path：パスを格納するバッファへのアドレス
 * url：URLが格納されたバッファへのアドレス
 * 戻り値：0
 */
int getHostnameAndPath(char *hostname, char *path, char *url) {
    unsigned int i;
    char hostname_path[MAX_HOSTNAME_SIZE + MAX_PATH_SIZE];

    /* URLの最初が"http://"の場合は"http://"を取り除く */
    if (strncmp(url, "http://", strlen("http://")) == 0) {
        sscanf(url, "http://%s", hostname_path);
    } else {
        strcpy(hostname_path, url);
    }

    /* 最初の'/'までの文字数をカウント */
    for (i = 0; i < strlen(hostname_path); i++) {
        if (hostname_path[i] == '/') {
            break;
        }
    }

    if (i == strlen(hostname_path)) {
        /* '/'がhostname_pathに含まれていなかった場合 */

        /* hostname_path全体をhostnameとする */
        strcpy(hostname, hostname_path);

        /* pathは'/'とする */
        strcpy(path, "/");
    } else {
        /* '/'がhostname_pathに含まれていなかった場合 */

        /* '/'の直前までをhostnameとする */
        strncpy(hostname, hostname_path, i);

        /* '/'以降をpathとする */
        strcpy(path, &hostname_path[i]);
    }

    return 0;
}

/*
 * リクエストメッセージを作成する
 * request_message：リクエストメッセージを格納するバッファへのアドレス
 * target：リクエストターゲット（リクストするファイル）
 * host：リクエスト先のホスト名
 * 戻り値：メッセージのサイズ
 */
int createRequestMessage(char *request_message, char *path, char *hostname) {

    char request[MAX_SIZE];
    char header[MAX_SIZE];

    sprintf(request, "GET %s HTTP/1.1", path);
    sprintf(header, "Host: %s\r\nConnection: close", hostname);
    sprintf(request_message, "%s\r\n%s\r\n\r\n", request, header);

    return strlen(request_message);
}

/*
 * リクエストメッセージを送信する
 * sock：接続済のソケット
 * request_message：送信するリクエストメッセージへのアドレス
 * message_size：送信するメッセージのサイズ
 * 戻り値：送信したデータサイズ（バイト長）
 */
int sendRequestMessage(int sock, char *request_message, unsigned int message_size) {

    int send_size;

    send_size = send(sock, request_message, message_size, 0);

    return send_size;
}

/*
 * レスポンスメッセージを受信する
 * sock：接続済のソケット
 * response_message：レスポンスメッセージを格納するバッファへのアドレス
 * buffer_size：↑のバッファのサイズ
 * 戻り値：受信したデータサイズ（バイト長）
 */
int recvResponseMessage(int sock, char *request_message, unsigned int buffer_size) {

    int total_recv_size = 0;
    int i;

    while (1) {
        int recv_size = recv(sock, &request_message[total_recv_size], buffer_size, 0);
        if (recv_size == -1) {
            printf("recv error\n");
            return -1;
        }
        if (recv_size <= 0) {
            printf("connection ended\n");
            break;
        }

        for (i = 0; i < recv_size; i++) {
            putc(request_message[total_recv_size + i], stdout);
        }
        total_recv_size += recv_size;
    }

    return total_recv_size;
}

/*
 * ボディをファイル保存する
 * file_path：保存先のファイルパス
 * response_message：レスポンスメッセージを格納するバッファへのアドレス
 * response_size：レスポンスメッセージのサイズ
 * 戻り値：成功時 0、失敗時 -1
 */
int saveBody(const char *file_path, char *response_message, unsigned int response_size) {

    FILE *fo;
    char *tmp;
    unsigned int skip_size = 0;

    /* レスポンスメッセージを空行まで読み飛ばし */
    tmp = strtok(response_message, "\n");
    while (tmp != NULL && (strcmp(tmp, "\r") != 0 && strcmp(tmp, "") != 0)) {
        skip_size += strlen(tmp) + strlen("\n");

        tmp = strtok(NULL, "\n");
        printf("%s\n", tmp);
    }

    if (tmp == NULL) {
        printf("body is not found\n");
        return -1;
    }

    /* 空行の次の行からをボディと見做してファイル保存 */
    skip_size += strlen(tmp) + strlen("\n");

    fo = fopen(file_path, "wb");
    if (fo == NULL) {
        printf("Open error (%s)\n", file_path);
        return -1;
    }

    fwrite(&response_message[skip_size], 1, response_size - skip_size, fo);

    fclose(fo);

    return 0;
}

/*
 * HTTP通信でサーバーとデータのやりとりを行う
 * sock：サーバーと接続済のソケット
 * hostname：ホスト名が格納されたバッファへのアドレス
 * path：パスが格納されたバッファへのアドレス
 * 戻り値：成功時 0、失敗時 -1
 */
int httpClient(int sock, char *hostname, char *path) {

    int request_size, response_size;
    char request_message[MAX_REQUEST_SIZE];
    char response_message[MAX_RESPONSE_SIZE];

    /* リクエストメッセージを作成 */
    request_size = createRequestMessage(request_message, path, hostname);
    if (request_size == -1) {
        printf("createRequestMessage error\n");
        return -1;
    }

    /* リクエストメッセージを送信 */
    if (sendRequestMessage(sock, request_message, request_size) == -1) {
        printf("sendRequestMessage error\n");
        return -1;
    }

    /* レスポンスメッセージを受信 */
    response_size = recvResponseMessage(sock, response_message, MAX_RESPONSE_SIZE);
    if (response_size == -1) {
        printf("recvResponseMessage error\n");
        return -1;
    }

    /* レスポンスメッセージに応じた処理（ボディのファイル保存のみ）
    if (saveBody("test.html", response_message, response_size) == -1) {
        printf("saveBody error\n");
        return -1;
    }*/
    printf("response_size: %i",response_size);
    return response_size;
}

struct results runTest(char* x){
    int sock = -1;
    struct sockaddr_in addr;
    unsigned short port = CONNECT_PORT;
    char url[MAX_URL];
    char *ip_address;

    int request_size, response_size;
    char request_message[MAX_REQUEST_SIZE];
    char response_message[MAX_RESPONSE_SIZE];

    char hostname[MAX_HOSTNAME_SIZE];
    char path[MAX_PATH_SIZE];
    struct timespec start, end, nend, uend;
	struct results result;
    strcpy(url, x);
    //initialize by -1 to represent packet loss
    result.ping = -1;
    result.uptime = -1;
    result.downtime = -1;
    result.reqSize = -1;
    result.repSize = -1;
    /* ホスト名とパスを取得 */
    getHostnameAndPath(hostname, path, url);

    /* IPアドレスを取得 */
    ip_address = getIpAddress(hostname);
    if (ip_address == NULL) {
        printf("getIPAddress error\n");
        exit(1);
    }

    /* ソケットを作成 */
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        printf("socket error\n");
        exit(1);
    }

    /* 構造体を全て0にセット */
    memset(&addr, 0, sizeof(struct sockaddr_in));

    /* 接続先の情報を設定 */
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    /* ip_addressは数値の配列なのでそのままコピー（inet_addrは不要） */
    memcpy(&(addr.sin_addr.s_addr), ip_address, 4);

	clock_gettime(CLOCK_MONOTONIC, &start);	/* mark start time */
    if (connect(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)) == -1) {
        printf("connect error\n");
        close(sock);
        exit(1);
    }
    clock_gettime(CLOCK_MONOTONIC, &end);	/* mark the end time */
    //calculate ping
    result.ping = (end.tv_sec - start.tv_sec) * (int64_t)1000000000
    + (end.tv_nsec - start.tv_nsec);
    //result.serverIP = ip_address;

    /* リクエストメッセージを作成 */
    request_size = createRequestMessage(request_message, path, hostname);
    if (request_size == -1) {
        printf("createRequestMessage error\n");
        exit(1);
    }

    clock_gettime(CLOCK_MONOTONIC, &start);	/* mark start time */
    /* リクエストメッセージを送信 */
    if (sendRequestMessage(sock, request_message, request_size) == -1) {
        printf("sendRequestMessage error\n");
        exit(1);
    }
    clock_gettime(CLOCK_MONOTONIC, &uend);	/* mark the end time */
    //calculate upload spd
    result.reqSize = request_size;
    result.uptime = (uend.tv_sec - start.tv_sec) * (int64_t)1000000000
    + (uend.tv_nsec - start.tv_nsec);

    clock_gettime(CLOCK_MONOTONIC, &start);	/* mark start time */
    /* レスポンスメッセージを受信 */
    response_size = recvResponseMessage(sock, response_message, MAX_RESPONSE_SIZE);
    if (response_size == -1) {
        printf("recvResponseMessage error\n");
        exit(1);
    }
    clock_gettime(CLOCK_MONOTONIC, &nend);	/* mark the end time */
    
    /* ソケット通信をクローズ */
    if (close(sock) == -1) {
        printf("close error\n");
        exit(1);
    }
    //calculate download spd
    result.repSize = response_size;
    result.downtime = (nend.tv_sec - start.tv_sec) * (int64_t)1000000000
    + (nend.tv_nsec - start.tv_nsec);

    return result;
}

int main(int argc, char *argv[]) {
    int sock = -1;
    struct sockaddr_in addr;
    unsigned short port = CONNECT_PORT;
    char url[MAX_URL];
    char *ip_address;

    char hostname[MAX_HOSTNAME_SIZE];
    char path[MAX_PATH_SIZE];

    if (argc == 2) {
        strcpy(url, argv[1]);
    } else {
        printf("set url dude\n");
        return -1;
    }

    /* ホスト名とパスを取得 */
    getHostnameAndPath(hostname, path, url);

    /* IPアドレスを取得 */
    ip_address = getIpAddress(hostname);
    if (ip_address == NULL) {
        printf("getIPAddress error\n");
        return -1;
    }

    /* ソケットを作成 */
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        printf("socket error\n");
        return -1;
    }

    /* 構造体を全て0にセット */
    memset(&addr, 0, sizeof(struct sockaddr_in));

    /* 接続先の情報を設定 */
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    /* ip_addressは数値の配列なのでそのままコピー（inet_addrは不要） */
    memcpy(&(addr.sin_addr.s_addr), ip_address, 4);

    /* サーバーに接続 */
    printf("Connect to %u.%u.%u.%u\n",
        (unsigned char)ip_address[0],
        (unsigned char)ip_address[1],
        (unsigned char)ip_address[2],
        (unsigned char)ip_address[3]
    );
    if (connect(sock, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)) == -1) {
        printf("connect error\n");
        close(sock);
        return -1;
    }
    /* HTTP通信でデータのやりとり */
    httpClient(sock, hostname, path);

    /* ソケット通信をクローズ */
    if (close(sock) == -1) {
        printf("close error\n");
        return -1;
    }

    return 0;
}