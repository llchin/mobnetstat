import 'dart:ffi';
import 'dart:io';
import 'package:ffi/ffi.dart';

class LocalTestResults extends Struct {
  @Int64()
  external int ping;

  @Int64()
  external int downspeed;

  @Int64()
  external int upspeed;

  @Int32()
  external int reqSize;

  @Int32()
  external int repSize;
}

typedef PingFunction = LocalTestResults Function(Pointer<Utf8> x);
typedef PingFunctionDart = LocalTestResults Function(Pointer<Utf8> x);

class FFIBridge {
  late PingFunctionDart getHTTPF;

  FFIBridge() {
    var dl = Platform.isAndroid
        ? DynamicLibrary.open('libclient.so')
        : Platform.isIOS
        ? DynamicLibrary.open('libclient.dylib'):
        DynamicLibrary.process();

    getHTTPF = dl
        .lookupFunction<
        PingFunction,
        PingFunctionDart>('runTest');
  }
  // serverTestResults getHTTP() => getHTTPF();

}
