import 'package:flutter/material.dart';
import 'TestSettings.dart';
import 'Others.dart';

void main() {
  runApp(
    const MaterialApp(
      home: HomePage(),
    ),
  );
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.dark,
        // primaryColor: Colors.blueGrey.shade200,
      ),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            bottom: const TabBar(
              tabs: [
                Tab(
                  text: 'test settings',
                ),
                Tab(
                  text: 'others',
                ),
              ],
            ),
            title: const Text('Mobnetstat'),
          ),
          body: const TabBarView(
            children: [
              TestSettings(),
              Others(),
            ],
          ),
        ),
      ),
    );
  }
}
