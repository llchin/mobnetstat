import 'dart:io';
import 'dart:convert';
import 'dart:async';

import 'ffi_bridge.dart';
import 'package:dart_ipify/dart_ipify.dart';
import 'package:ffi/ffi.dart';
import 'package:share_plus/share_plus.dart';
import 'package:flutter/material.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:statistics/statistics.dart';
import 'package:path_provider/path_provider.dart';

final List<String> US = [
  "example.com",
  "yahoo.com",
  "intel.com",
  "kekkonen.neocities.org",
  "usdebtclock.org",
];
final List<String> HK = [
  "rthk.hk",
  "hkjc.com",
  "hkstp.org",
  "philosophy.hku.hk",
  "edcity.hk"
];
DateTime start = DateTime.now();
DateTime end = DateTime.now();
String _timepassed = "";
FinalTestRecords _testRecords = FinalTestRecords();

late String _current = '';
bool cont = true;
String fpath = '';

class TestProcess extends StatefulWidget {
  final String location;
  final String cinterval;
  final int tries;
  final int hours;
  final int mins;
  const TestProcess(
      {Key? key,
      required this.location,
      required this.tries,
      required this.hours,
      required this.mins,
      required this.cinterval})
      : super(key: key);
  @override
  _TestProcessState createState() => _TestProcessState();
}

class _TestProcessState extends State<TestProcess> {
  final FFIBridge _ffiBridge = FFIBridge();
  late LocalTestResults _localTestResults;

  @override
  void initState() {
    super.initState();
    setState(() {
      start = DateTime.now();
      end = start.add(Duration(hours: widget.hours, minutes: widget.mins));
      _testRecords = FinalTestRecords();
      _timepassed = start.elapsedTime.toString();
      cont = true;
    });
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      setState(() {
        _current = "getting network data";
      });
      if(await networkFetch()){
        setState(() {
          _current = "testing";
        });
      };
      if (widget.location == "US") {
        for (var desDomain in US) {
          var temp = await InternetAddress.lookup(desDomain,
              type: InternetAddressType.IPv4);
          ServerTestResult s = ServerTestResult(desDomain, temp[0].address);
          _testRecords.serverTestResult.add(s);
        }
      } else if (widget.location == "HK") {
        for (var desDomain in HK) {
          var temp = await InternetAddress.lookup(desDomain,
              type: InternetAddressType.IPv4);
          ServerTestResult s = ServerTestResult(desDomain, temp[0].address);
          _testRecords.serverTestResult.add(s);
        }
      }

      if (widget.tries != 0) {
        if (widget.location == "US") {
          for (var i = 1; i < widget.tries + 1; i++) {
            setState(() {
              _current = "testing\n\n count: $i";
              _timepassed = start.elapsedTime.toString();
            });
            for (var j = 0; j < US.length; j++) {
              await fetch(US[j], j);
            }
          }
        } else if (widget.location == "HK") {
          for (var i = 1; i < widget.tries + 1; i++) {
            setState(() {
              _current = "testing\n\n count: $i";
              _timepassed = start.elapsedTime.toString();
            });
            for (var j = 0; j < HK.length; j++) {
              await fetch(HK[j], j);
            }
          }
        }
      } else if (widget.hours > 0 || widget.mins > 0) {
        if (widget.location == "US") {
          do {
            setState(() {
              _timepassed = start.elapsedTime.toString();
            });
            for (var i = 0; i < US.length; i++) {
              setState(() {
                _current = "testing\n\n count: $i";
              });
              await fetch(US[i], i);
            }
          } while (cont && DateTime.now().isBefore(end));
        } else if (widget.location == "HK") {
          do {
            setState(() {
              _timepassed = start.elapsedTime.toString();
            });
            for (var i = 0; i < HK.length; i++) {
              await fetch(HK[i], i);
            }
          } while (cont && DateTime.now().isBefore(end));
        }
      } else {
        if (widget.location == "US") {
          do {
            setState(() {
              _timepassed = start.elapsedTime.toString();
            });
            for (var i = 0; i < US.length; i++) {
              await fetch(US[i], i);
            }
          } while (cont);
        } else if (widget.location == "HK") {
          do {
            setState(() {
              _timepassed = start.elapsedTime.toString();
            });
            for (var i = 0; i < HK.length; i++) {
              await fetch(HK[i], i);
            }
          } while (cont);
        }
      }
      setState(() {
        _current = "analyzing";
      });
      await networkSpecs();
    });
  }

  //run the test and save the results
  Future fetch(String location, int i) async {
    await Future.delayed(const Duration(milliseconds: 1));
    _localTestResults = _ffiBridge.getHTTPF(location.toNativeUtf8());
    _testRecords.serverTestResult[i].pings.add(_localTestResults.ping);
    _testRecords.serverTestResult[i].uploads.add(_localTestResults.upspeed);
    _testRecords.serverTestResult[i].downloads.add(_localTestResults.downspeed);
    _testRecords.serverTestResult[i].reqSize.add(_localTestResults.reqSize);
    _testRecords.serverTestResult[i].repSize.add(_localTestResults.repSize);
  }

  Future networkFetch() async {
    // await Future.delayed(const Duration(seconds: 5));
    _testRecords.time = DateTime.now().toString();
    _testRecords.cinterval = widget.cinterval;
    _testRecords.location = widget.location;
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      _testRecords.deviceName = androidInfo.manufacturer.toString() +
          " " +
          androidInfo.device.toString();
      _testRecords.deviceOS =
          "Android " + androidInfo.version.release.toString();
      print('Running on ${_testRecords.deviceOS}'); // e.g. "Moto G (4)"

    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      _testRecords.deviceName = iosInfo.utsname.machine.toString();
      _testRecords.deviceOS = "IOS " + iosInfo.utsname.release.toString();
      print('Running on ${iosInfo.utsname.machine}'); // e.g. "iPod7,1"
    }
    _testRecords.deviceIP = await Ipify.ipv4();
    print(_testRecords.deviceIP);
    var x = await (Connectivity().checkConnectivity());
    // print(x.name);
    _testRecords.connectivity = x.name;
    return true;
  }

  Future networkSpecs() async {
    const double b = 8.0;
    const double m = 1000000.0;
    const double N = 1000000000.0;
    List<double> allping = [];
    List<double> alldownspeed = [];
    List<double> allupspeed = [];
    int totalpacketsent = 0;
    int totalpacketloss = 0;

    //load data inside each server and identified failed packets
    _testRecords.endtime = DateTime.now().toString();
    for (var a = 0; a < _testRecords.serverTestResult.length; a++) {
      // print(_testRecords.serverTestResult[a].reqSize);
      // print(_testRecords.serverTestResult[a].repSize);
      setState(() {
        _timepassed = start.elapsedTime.toString();
      });
      int packetloss = 0;
      int packetsent = 0;
      List<double> vping = [];
      List<double> downspeed = [];
      List<double> upspeed = [];
      //check packet loss for each server
      for (var i = 0; i < _testRecords.serverTestResult[a].pings.length; i++) {
        if (_testRecords.serverTestResult[a].pings[i] == -1) {
          _testRecords.serverTestResult[a].packetLoss++;
          packetloss++;
          continue;
        }
        if (_testRecords.serverTestResult[a].uploads[i] == -1 ||
            _testRecords.serverTestResult[a].reqSize[i] == -1) {
          _testRecords.serverTestResult[a].packetLoss++;
          packetloss++;
          continue;
        }
        packetsent++; // add packet sent count
        if (_testRecords.serverTestResult[a].downloads[i] == -1 ||
            _testRecords.serverTestResult[a].repSize[i] == -1) {
          _testRecords.serverTestResult[a].packetLoss++;
          packetloss++;
          continue;
        }
        vping.add((_testRecords.serverTestResult[a].pings[i] / N) *
            1000); //save as ms
        upspeed.add((_testRecords.serverTestResult[a].reqSize[i] *
            b /
            (_testRecords.serverTestResult[a].uploads[i] / N)) /
            m); //save as Mbps
        downspeed.add((_testRecords.serverTestResult[a].repSize[i] *
                b /
                (_testRecords.serverTestResult[a].downloads[i] / N)) /
            m); //save as Mbps
      }
      _testRecords.serverTestResult[a].packetsent = packetsent;
      // _testRecords.serverTestResult[a].vping = vping;
      // _testRecords.serverTestResult[a].downspeed = downspeed;
      // _testRecords.serverTestResult[a].upspeed = upspeed;
      _testRecords.serverTestResult[a].jitter = jitter(vping);
      // _testRecords.tests = widget.tries;
      //https://pub.dev/packages/statistics sd, median, mean, max, min can be found here
      _testRecords.serverTestResult[a].pingStats = Stats(
          vping.mean,
          vping.statistics.median.toDouble(),
          vping.statistics.medianLow.toDouble(),
          vping.statistics.medianHigh.toDouble(),
          vping.statistics.min,
          vping.statistics.max,
          vping.standardDeviation,
          marginOfError(vping, widget.cinterval));
      _testRecords.serverTestResult[a].downspeedStats = Stats(
          downspeed.mean,
          downspeed.statistics.median.toDouble(),
          downspeed.statistics.medianLow.toDouble(),
          downspeed.statistics.medianHigh.toDouble(),
          downspeed.statistics.min,
          downspeed.statistics.max,
          downspeed.standardDeviation,
          marginOfError(downspeed, widget.cinterval));
      _testRecords.serverTestResult[a].upspeedStats = Stats(
          upspeed.mean,
          upspeed.statistics.median.toDouble(),
          upspeed.statistics.medianLow.toDouble(),
          upspeed.statistics.medianHigh.toDouble(),
          upspeed.statistics.min,
          upspeed.statistics.max,
          upspeed.standardDeviation,
          marginOfError(upspeed, widget.cinterval));
      for (double i in vping) {
        allping.add(i);
      }
      for (double i in downspeed) {
        alldownspeed.add(i);
      }
      for (double i in upspeed) {
        allupspeed.add(i);
      }
      // print(_testRecords.serverTestResult[a].vping);
      totalpacketsent += packetsent;
      totalpacketloss += packetloss;
    }
    ;
    //get the combined data and get the overall result
    _testRecords.overallTestResult = OverallTestResult();
    _testRecords.overallTestResult.pingStats = Stats(
        allping.mean,
        allping.statistics.median.toDouble(),
        allping.statistics.medianLow.toDouble(),
        allping.statistics.medianHigh.toDouble(),
        allping.statistics.min,
        allping.statistics.max,
        allping.standardDeviation,
        marginOfError(allping, widget.cinterval));
    _testRecords.overallTestResult.downspeedStats = Stats(
        alldownspeed.mean,
        alldownspeed.statistics.median.toDouble(),
        alldownspeed.statistics.medianLow.toDouble(),
        alldownspeed.statistics.medianHigh.toDouble(),
        alldownspeed.statistics.min,
        alldownspeed.statistics.max,
        alldownspeed.standardDeviation,
        marginOfError(alldownspeed, widget.cinterval));
    _testRecords.overallTestResult.upspeedStats = Stats(
        allupspeed.mean,
        allupspeed.statistics.median.toDouble(),
        allupspeed.statistics.medianLow.toDouble(),
        allupspeed.statistics.medianHigh.toDouble(),
        allupspeed.statistics.min,
        allupspeed.statistics.max,
        allupspeed.standardDeviation,
        marginOfError(allupspeed, widget.cinterval));

    _testRecords.overallTestResult.jitter = jitter(allping);
    _testRecords.overallTestResult.packetsent = totalpacketsent;
    _testRecords.overallTestResult.packetLoss = totalpacketloss;

    //save data into class https://www.bezkoder.com/dart-flutter-convert-object-to-json-string/
    // encode
    print(_testRecords.toJson());
    String json = jsonEncode(_testRecords);
    print(json);
    // var myFile = File(path);
    // print(myFile);
    // Directory appDocDir = await getApplicationDocumentsDirectory();
    // String appDocPath = appDocDir.path;
    // Directory(appDocPath+'/'+path).create(recursive: true)
    // // The created directory is returned as a Future.
    //     .then((Directory directory) {
    //   print('Path of New Dir: '+directory.path);
    // });

    File file = File(await getFilePath()); // 1
    file.writeAsString(json); // 2
    print(file.path);
    // await Share.share(json, subject: 'test result');
    await Share.shareFiles([file.path], subject: 'test result');

    String fileContent = await file.readAsString(); // 2

    print('File Content: $fileContent');
    const snackBar = SnackBar(
      content: const Text("testing done"),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    Navigator.pop(context);
  }

  Future<String> getFilePath() async {
    fpath = _testRecords.time.toString().replaceAll(" ", "_") + ".json";
    print(await getExternalStorageDirectory());
    Directory? appDocumentsDirectory = await getExternalStorageDirectory(); // 1
    String? appDocumentsPath = appDocumentsDirectory?.path; // 2
    String filePath = '$appDocumentsPath/$fpath'; // 3
    print(filePath);
    return filePath;
  }

  double jitter(List<double> theList) {
    double testjitter = 0;
    for (var i = 1; i < theList.length; i++) {
      testjitter += (theList[i] - theList[i - 1]).abs();
    }
    testjitter = testjitter / (theList.length - 1);
    return testjitter;
  }

  double marginOfError(List<double> theList, String c) {
    var z = 0.0;
    switch (c) {
      case "99%":
        {
          z = 2.576;
          break;
        }
      case "95%":
        {
          z = 1.960;
          break;
        }
      default:
        z = 1.645;
    }
    // print(theList);
    return z * (theList.standardDeviation / theList.length.squareRoot);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //     // title: const Text('Second Route'),
      //     ),
      body: Column(
        children: [
          Row(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.centerLeft,
                child: Text(
                  'Server location: ' + widget.location,
                  style: const TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.centerLeft,
                child: Text(
                  'Now running: ' + _current,
                  style: const TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.1,
                width: MediaQuery.of(context).size.width,
                alignment: Alignment.centerLeft,
                child: Text(
                  'test elapsed time: ' + _timepassed,
                  style: const TextStyle(fontSize: 20),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.4,
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                style:
                    ElevatedButton.styleFrom(minimumSize: const Size(150, 100)),
                onPressed: () {
                  setState(() {
                    cont = false;
                  });
                },
                child:
                    const Text('STOP TESTING', style: TextStyle(fontSize: 36)),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class FinalTestRecords {
  late String time, endtime;
  late String deviceIP;
  late String connectivity;
  late String deviceName;
  late String deviceOS;
  late String cinterval;
  late String location;
  late List<ServerTestResult> serverTestResult = [];
  late OverallTestResult overallTestResult;
  Map<String, dynamic> toJson() {
    return {
      'test start time': time,
      'test end time': endtime,
      'device IP': deviceIP,
      'connectivity': connectivity,
      'device name': deviceName,
      'device OS': deviceOS,
      'confidence interval': cinterval,
      'servers location': location,
      'overall test result': overallTestResult.toJson(),
      'testResult1': serverTestResult[0].toJson(),
      'testResult2': serverTestResult[1].toJson(),
      'testResult3': serverTestResult[2].toJson(),
      'testResult4': serverTestResult[3].toJson(),
      'testResult5': serverTestResult[4].toJson(),
    };
  }
}

class OverallTestResult {
  late List<double> vping = [];
  late List<double> downspeed = [];
  late List<double> upspeed = [];

  late Stats pingStats, downspeedStats, upspeedStats;
  late double jitter;
  late int packetsent, packetLoss;

  Map<String, dynamic> toJson() => {
        'packet sent': packetsent,
        'ping (ms)': pingStats.toJson(),
        'download speed (mb/s)': downspeedStats.toJson(),
        'upload speed (mb/s)': upspeedStats.toJson(),
        'jitter': jitter,
        'packet loss count': packetLoss,
      };
}

class ServerTestResult {
  late List<int> pings = [];
  late List<int> uploads = [];
  late List<int> downloads = [];
  late List<int> reqSize = [];
  late List<int> repSize = [];

  late List<double> vping = [];
  late List<double> downspeed = [];
  late List<double> upspeed = [];
  //output
  late String desIP;
  late String desDomain;
  late Stats pingStats, downspeedStats, upspeedStats;
  late double jitter;
  late int packetsent, packetLoss = 0;

  ServerTestResult(this.desIP, this.desDomain);

  Map<String, dynamic> toJson() => {
        'destinationIP': desIP,
        'destination domain': desDomain,
        'packet sent': packetsent,
        'ping (ms)': pingStats.toJson(),
        'download speed (mb/s)': downspeedStats.toJson(),
        'upload speed (mb/s)': upspeedStats.toJson(),
        'jitter': jitter,
        'packet loss count': packetLoss,
      };
}

class Stats {
  late double mean;
  late double median;
  late double medianLow;
  late double medianHigh;
  late double min;
  late double max;
  late double sd;
  late double moe;
  Stats(this.mean, this.median, this.medianLow, this.medianHigh, this.min,
      this.max, this.sd, this.moe);

  Map<String, dynamic> toJson() => {
        'mean': mean,
        'median': median,
        'median low': medianLow,
        'median high': medianHigh,
        'min': min,
        'max': max,
        'standard deviation': sd,
        'margin of error': moe,
      };
}
