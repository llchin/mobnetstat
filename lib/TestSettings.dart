import 'dart:async';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'dart:developer' as developer;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'TestProcess.dart';

String _location = 'US';
int _tries = 0;
int _hours = 0;
int _mins = 0;
String _cinterval = "90%";

class TestSettings extends StatefulWidget {
  const TestSettings({Key? key}) : super(key: key);

  @override
  _TestSettingsState createState() => _TestSettingsState();
}

class _TestSettingsState extends State<TestSettings> {
  ConnectivityResult _connectionStatus = ConnectivityResult.none;
  final Connectivity _connectivity = Connectivity();
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the widget tree.
    // This also removes the _printLatestValue listener.
    _connectivitySubscription.cancel();
    super.dispose();
  }

  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }
    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            child: ListView(
              children: [
                ListTile(
                  leading: _connectionStatus != ConnectivityResult.none?const Icon(Icons.wifi):const Icon(Icons.wifi_off),
                    title:Text('Connection Status: ${_connectionStatus.toString()}'),
                ),
                ListTile(
                  leading: const Icon(Icons.map),
                  title: Text('Server Location: ' + _location),
                  trailing: PopupMenuButton(
                    onSelected: (String l) {
                      setState(() {
                        _location = l;
                      });
                      print(_location);
                    },
                    itemBuilder: (BuildContext context) =>
                        <PopupMenuEntry<String>>[
                      const PopupMenuItem<String>(
                        value: "US",
                        child: Text('United States'),
                      ),
                      const PopupMenuItem<String>(
                        value: "HK",
                        child: Text('Hong Kong'),
                      ),
                      // const PopupMenuItem<String>(
                      //   value: "UK",
                      //   child: Text('United Kingdom'),
                      // ),
                    ],
                  ),
                ),
                ListTile(
                  leading: const Icon(Icons.workspaces_outline),
                  title: Text('Confidence interval: ' + _cinterval.toString()),
                  trailing: PopupMenuButton(
                    onSelected: (String l) {
                      setState(() {
                        _cinterval = l;
                      });
                      print(_cinterval);
                    },
                    itemBuilder: (BuildContext context) =>
                    <PopupMenuEntry<String>>[
                      const PopupMenuItem<String>(
                        value: "90%",
                        child: Text("90%"),
                      ),
                      const PopupMenuItem<String>(
                        value: "95%",
                        child: Text("95%"),
                      ),
                      const PopupMenuItem<String>(
                        value: "99%",
                        child: Text("99%"),
                      ),
                    ],
                  ),
                ),
                ListTile(
                  leading: const Icon(Icons.wifi_protected_setup),
                  title: Text('Number of trials: (0 to run indefinitely)' + _tries.toString()),
                  trailing: Container(
                    width: 150.0,
                    height: 10.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: TextField(
                            onChanged: (String t) {
                              // print(t);
                              if(int.parse(t)>=0){
                                setState(() {
                                  _tries = int.parse(t);
                                });
                              }
                            },
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.digitsOnly
                            ], // Only numbers can be entered
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                ListTile(
                  leading: const Icon(Icons.wifi_protected_setup),
                  title: Text('Maximum test time(hours): '+_hours.toString()),
                  trailing: Container(
                    width: 150.0,
                    height: 100.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: TextField(
                            onChanged: (String t) {
                              if(int.parse(t)>=0){
                                setState(() {
                                  _hours = int.parse(t);
                                });
                              }
                            },
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.digitsOnly
                            ], // Only numbers can be entered
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                ListTile(
                  leading: const Icon(Icons.wifi_protected_setup),
                  title: Text('test time(0-60 mins): ' + _mins.toString()),
                  trailing: Container(
                    width: 150.0,
                    height: 100.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: TextField(
                            onChanged: (String t) {
                              if(int.parse(t)>=0 && int.parse(t)<=60){
                                setState(() {
                                  _mins = int.parse(t);
                                });
                              }
                            },
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.number,
                            inputFormatters: <TextInputFormatter>[
                              FilteringTextInputFormatter.digitsOnly
                            ], // Only numbers can be entered
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.1,
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                child: const Text('Start',style: TextStyle(fontSize: 36)),
                style: ElevatedButton.styleFrom(minimumSize: const Size(150,100)),
                onPressed: () {
                  if(_connectionStatus != ConnectivityResult.none){
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                            TestProcess(location: _location, tries: _tries,hours: _hours,
                              mins: _mins,cinterval: _cinterval,)));
                  }
                  else{
                    final snackBar = SnackBar(
                      content: const Text('ERROR! NO INTERNET'),
                    );
                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
